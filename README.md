# 3DCV Final Project #

This is the course final project for [3D computer vision](https://www.cs.utah.edu/~srikumar/cv_spring2017.htm) in University of Utah 2017

### Repository Structure ###

* model: collection of all OBJ mesh models we are using
* renderer: scrpits for rendering images using [OSPRay](https://github.com/wilsonCernWq/ospray/tree/qwu-renderer)
* voxelizer: voxelization program
* network: the network is implemented in our [github page](https://github.com/wilsonCernWq/RRGAN)
* generated dataset: [download](https://bitbucket.org/WilsonOverCloud/2017-3dcv-project/downloads/image%20data.zip)
* report: [download](https://bitbucket.org/WilsonOverCloud/2017-3dcv-project/downloads/project%20report.zip)

### model ###

There contains 12 examples OBJ files including fruits and backgrounds.

### renderer ###

In order to use the renderer, you need to have OSPRay installed and compiled with [Imagemagick](https://www.imagemagick.org/script/index.php) C++ headers. 
After that, you can start to compile ospray from source.
In general you need to have several packages downloaded or installed. They are:
```
[ISPC](https://ispc.github.io/downloads.html)
[Embree](https://embree.github.io/downloads.html)
[TBB](https://www.threadingbuildingblocks.org/)
[Qt](https://www.qt.io/download/)
```
Then you need to pull our Github repository from this [link](https://github.com/wilsonCernWq/ospray/tree/qwu-renderer):
```
git clone https://github.com/wilsonCernWq/ospray.git 
cd ospray
```
And edit the `premake.sh` file in root. This is an exmaple of how to edit the file:
```
...
# change this block to fit your environment
DIRROOT=~
OSPROOT=~/OSPRay
export QT_ROOT=${DIRROOT}/visitOSPRay/qt-everywhere-opensource-src-4.8.3
export TBB_ROOT=${OSPROOT}/tbb2017_20160916oss
export ISPC_ROOT=${OSPROOT}/ispc-v1.9.1-linux
export EMBREE_ROOT=${OSPROOT}/embree-2.14.0.x86_64.linux
export INTELICC_PATH=/opt/intel/bin/icc
source /opt/intel/parallel_studio_xe_2017.0.035/bin/psxevars.sh intel64

# Qt component
export PATH=${QT_ROOT}/bin:${PATH}
export LD_LIBRARY_PATH=${QT_ROOT}/lib:${LD_LIBRARY_PATH}

# TBB component
source ${TBB_ROOT}/bin/tbbvars.sh intel64
export LD_LIBRARY_PATH=${TBB_ROOT}/lib/intel64/gcc4.7:${LD_LIBRARY_PATH}

# ispc component
export PATH=${ISPC_ROOT}:${PATH}
...
```
Then you should be able to build ans install OSPRay by running:
```
./premake.sh -im -e -a -DCMAKE_INSTALL_PREFIX=/path/to/your/install/folder
cd build_imagemagick_embree
make -j8
make install
cd ..
```
For running OSPRay, you need to set environmental variables in advance. In this repository `renderer/source_ospray.sh`, 
we provided an example for setting environmental variables. You need to change the `bin` path, `lib` path and embree path inside the file
```
#!/bin/bash
export PATH=$PATH:/home/sci/qwu/software/ospray-qwu-28.03.2017/bin/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/sci/qwu/software/ospray-qwu-28.03.2017/lib64/
source ~/OSPRay/embree-2.14.0.x86_64.linux/embree-vars.sh 
```
Then you should be able to run OSPRay after calling `source source_ospray.sh`

In order to generate images using our script, you need to go into `renderer/run.sh` and modify the first two lines:
```
MODELPATH=/home/sci/qwu/Projects/3DCV/model
OSPRAYPATH=/home/sci/qwu/software/ospray-qwu-28.03.2017/bin
```
Then call `./run.sh` directly.

The `run.sh` file specifies parameters for generating images
```
${OSPRAYPATH}/ospGlutRenderer                          \
    ${MODELPATH}/crytek-sponza/sponza.obj              \
    --sun-dir -.3 -1 -.04                              \
    --renderer pt                                      \
    --sun-int 12                                       \
    -vp -1040.681274 550.735718 64.358147              \
    -vu 0.000000 1.000000 0.000000                     \
    -vi -990.000000 520.000000 40.000004               \
    --hdri-light 1 ${MODELPATH}/rnl_probe.pfm          \
    ${FILE}                                            \
    -ao 0                                              \
    --ambient .85 .9 1 .3                              \
    --multiObjects                                     \
    --translate ${TRANSX} ${TRANSY} ${TRANSZ}          \
    --scale ${SCALE} ${SCALE} ${SCALE}                 \
    --maxFrame 100 example
```
This line specifies the number of frames that the program will renderer and the filename the screenshot will be saved into.
```
--maxFrame <max-frame-count> <image-file-name>
```

### voxelizer ###

Useful script is in example folder. voxelizer.out is used to voxelize model with input OBJ file. The output raw file would be used to convert to binvox for voxel viewer.  

Voxeliztion:
```
./voxelizer [OPTIONS] file.obj resolution precision
	OPTIONS:
		-voxel[default]	 output in voxel
		-obj		 output mesh data in OBJ format
		-txt		 output point cloud in TXT format
		-o <filename>	 output filename
```
 A voxel viewer is in binvox folder. Convert raw format to binvox using raw2binvox.py.

Convert to binvox:
```
python raw2binvox.py <filename-without-suffix>
```
Voxel viewer:
```
./viewvox <binvox-file>
```