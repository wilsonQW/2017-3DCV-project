#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <limits>

struct vec
{
    float x;
    float y;
    float z;
};

struct box
{
    vec lower;
    vec upper;
    std::string l;
    float maxDiff;
};

struct resolution
{
  float resX;
  float resY;
  float resZ;

};
/*
* ! LOAD OBJ FILE
*/
bool loadObj( tinyobj::attrib_t* attrib, std::vector<tinyobj::shape_t>* shapes, std::vector<tinyobj::material_t>* materials, std::string* err, const char* filename)
{
        bool ret = tinyobj::LoadObj(attrib, shapes, materials, err, filename);
        if (!err -> empty())
         {
             printf("err: %s\n", err -> c_str());
         }

        if (!ret)
        {
             printf("failed to load : %s\n", filename);
             return false;
         }

         if (shapes -> size() == 0)
         {
             printf("err: # of shapes are zero.\n");
             return false;
         }

         return ret;
}

/*
**  BOUDING BOX
*/
box bbox( tinyobj::attrib_t *attrib)
{
    box bbox;
    bbox.lower.x = std::numeric_limits<float>::max();
    bbox.lower.y = std::numeric_limits<float>::max();
    bbox.lower.z = std::numeric_limits<float>::max();
    bbox.upper.x = std::numeric_limits<float>::min();
    bbox.upper.y = std::numeric_limits<float>::min();
    bbox.upper.z = std::numeric_limits<float>::min();
    for (size_t i = 0; i < attrib -> vertices.size() / 3; i++)
    {
        bbox.lower.x = std::min(bbox.lower.x, attrib -> vertices[ 3 * i + 0]);
        bbox.lower.y = std::min(bbox.lower.y, attrib -> vertices[ 3 * i + 1]);
        bbox.lower.z = std::min(bbox.lower.z, attrib -> vertices[ 3 * i + 2]);
        bbox.upper.x = std::max(bbox.upper.x, attrib -> vertices[ 3 * i + 0]);
        bbox.upper.y = std::max(bbox.upper.y, attrib -> vertices[ 3 * i + 1]);
        bbox.upper.z = std::max(bbox.upper.z, attrib -> vertices[ 3 * i + 2]);
    }

    float diffX = bbox.upper.x - bbox.lower.x;
    float diffY = bbox.upper.y - bbox.lower.y;   
    float diffZ = bbox.upper.z - bbox.lower.z;
    if(diffX > diffY && diffX > diffZ){
        bbox.l = "x";
        bbox.maxDiff = diffX;
    }
    else if(diffY > diffZ && diffY > diffX){
        bbox.l = "y";
        bbox.maxDiff = diffY;
    }
    else{
        bbox.l = "z";
        bbox.maxDiff = diffZ;
    }
    return bbox;
}
/*
** SORT VERTEX
*/
bool sortVertex(vx_vertex_t vi, vx_vertex_t vj) {
    if (vi.z < vj.z) { return true; }
    else if (vi.z == vj.z) {
	if (vi.y < vj.y) { return true; }
	else if (vi.y == vj.y) {
	    return vi.x < vj.x;
	}
	else { return false; }
    }
    else { return false; }
}
