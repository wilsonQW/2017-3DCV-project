#!/usr/bin/python

import sys
import re
import binvox_rw
import numpy as np

def parse(pattern, string):
    return re.match(pattern, string).groups(0)

def stof(array):
    tmp = [];
    for i in array:
        tmp.append(float(i))
    return tmp

def stoi(array):
    tmp = [];
    for i in array:
        tmp.append(int(i))
    return tmp

# read RAW input file
fraw = open(sys.argv[1] + ".raw", "rb")
data = np.fromfile(fraw, dtype=np.uint8).astype(np.bool)

# read PC input file
ftxt = open(sys.argv[1] + ".txt", "r")
lines = ftxt.readlines()

# read properties
dims   = stoi(parse(r'^dimensions: ([-+]?\d+)\t([-+]?\d+)\t([-+]?\d+)$', lines[3]))
translate = stof(parse(r'^translation: ([-+]?\d*\.?\d+|\d+)\t([-+]?\d*\.?\d+|\d+)\t([-+]?\d*\.?\d+|\d+)$', lines[4]))
scale     = stof(parse(r'^scale: ([-+]?\d*\.?\d+|\d+)$', lines[5]))[0]

print('dims: ', dims[0], dims[1], dims[2])

# create model
data3d = np.zeros((dims[0],dims[2],dims[1]), dtype=np.bool)
for z in range(0, dims[2]):
    for y in range(0, dims[1]):
        for x in range(0, dims[0]):
            id = z * dims[1] * dims[0] + y * dims[0] + x
            data3d[x][z][y] = data[id]

model = binvox_rw.Voxels(data3d, (dims[0],dims[2],dims[1]), translate, scale, "xzy")
model.write(open(sys.argv[1] + ".binvox", "wb"))
