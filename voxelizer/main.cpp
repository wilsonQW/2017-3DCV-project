#define VOXELIZER_IMPLEMENTATION
#include "voxelizer.h"

#include "helper.h"
#include "export.h"
#include <iostream>
#include <sstream>


int main( int argc, char** argv )
{
    if (argc < 4)
     {
       std::cout << "Usage: " << std::endl;
       std::cout << "\t./voxelizer file.obj dimension precision outputFilename" << std::endl;
       std::cout << "\t\t-o <filename>\t output filename" << std::endl;
       return EXIT_FAILURE;
    }

  /*
  ** PARSE COMMAND LINE
   */
   const char* filename = argv[1];
   int dimension = atoi(argv[2]);
   float precision = atof(argv[3]);
   std::string outputFilename = std::string(argv[4]);
    /*
    ** LOAD FILE
    */
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string err;
    bool ret = loadObj(&attrib, &shapes, &materials, &err, filename);

    /*
    ** BOUNDING BOX
    */
    box mbbox;
    mbbox =  bbox(&attrib);
    std::cout << "the largest distance: " << mbbox.maxDiff << std::endl;
    std::cout << "the largest distance coord: " << mbbox.l<< std::endl;    

    // voxel size -- resolution
    // resolution res;
    
    // res.resX = (mbbox.upper.x - mbbox.lower.x) / dimension;
    // res.resY = (mbbox.upper.y - mbbox.lower.y) / dimension;
    // res.resZ = (mbbox.upper.z - mbbox.lower.z) / dimension;
    resolution res;
    if(mbbox.l == "x") {
            res.resX = (mbbox.upper.x - mbbox.lower.x) / dimension;
            res.resY = (mbbox.upper.x - mbbox.lower.x) / dimension;
            res.resZ = (mbbox.upper.x - mbbox.lower.x) / dimension;
    }else if(mbbox.l == "y"){
            res.resX = (mbbox.upper.y - mbbox.lower.y) / dimension;
            res.resY = (mbbox.upper.y - mbbox.lower.y) / dimension;
            res.resZ = (mbbox.upper.y - mbbox.lower.y) / dimension;
    }else{
            res.resX = (mbbox.upper.z - mbbox.lower.z) / dimension;
            res.resY = (mbbox.upper.z - mbbox.lower.z) / dimension;
            res.resZ = (mbbox.upper.z - mbbox.lower.z) / dimension;
    }
    /*
     * COMPUTE MESH ONLY USE FIRST SHAPE
     */
    std::cout << "# of shapes: " << shapes.size() << std::endl;

     vx_point_cloud_t* result;

    //for (size_t i = 0; i < shapes.size(); i++) {
        vx_mesh_t* mesh;

        mesh = vx_mesh_alloc(attrib.vertices.size(), shapes[0].mesh.indices.size());

        for (size_t f = 0; f < shapes[0].mesh.indices.size(); f++) {
            mesh->indices[f] = shapes[0].mesh.indices[f].vertex_index;
        }

        for (size_t v = 0; v < attrib.vertices.size() / 3; v++) {
            mesh->vertices[v].x = attrib.vertices[3*v+0];
            mesh->vertices[v].y = attrib.vertices[3*v+1];
            mesh->vertices[v].z = attrib.vertices[3*v+2];
        }
        std::cout << " resolution: " << res.resX << " " << res.resY << " " << res.resZ;
        std::cout << " mesh vertices: " << mesh -> vertices[0].x << std::endl;

        result = vx_voxelize_pc(mesh, res.resX, res.resY, res.resZ, precision);

        // save point list

        std::vector<vx_vertex_t> list;

        std::cout << " number of vertices: " << result -> nvertices << std::endl;

	       for (int j = 0; j < result->nvertices; ++j)
         {
           list.push_back(result->vertices[j]);
         }
        // sort result
        std::sort(list.begin(), list.end(), sortVertex);
        std::cout << " -- Done sorting\n" << std::endl;

    /*
    ** WRITE TXT HEADER
    */
    writeTxtHeader(outputFilename, mbbox, dimension, precision, res);
    writeRaw(outputFilename, mbbox, dimension, res, list);
    /*
    ** FILL THE OBJECT'S INTERNAL
    */



  return ret ? EXIT_SUCCESS : EXIT_FAILURE;
}
