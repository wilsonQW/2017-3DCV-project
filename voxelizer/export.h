#include <fstream>
#include <algorithm>

bool writeTxtHeader(std::string outputFilename, box mbbox, int dimension, float precision, resolution res)
{

	int nX = (mbbox.upper.x - mbbox.lower.x )/ res.resX ;
	int nY = (mbbox.upper.y - mbbox.lower.y) / res.resY ;
	int nZ = (mbbox.upper.z - mbbox.lower.y) / res.resZ;


  std::ofstream fileTXT((outputFilename + std::string(".txt")).c_str());
  fileTXT << "# " << outputFilename << "\n";
  // fileTXT << "ouput file format: " << "OSX" << "\n";
  fileTXT << "bounding box upper: " << mbbox.upper.x << "\t"
                                  << mbbox.upper.y << "\t"
                                  << mbbox.upper.z << "\n";
  fileTXT << "bounding box lower: " << mbbox.lower.x << "\t"
                                  << mbbox.lower.y << "\t"
                                  << mbbox.lower.z << "\n";
  fileTXT << "dimensions: " 
  							<< nX << "\t"
                            << nY << "\t"
                            << nZ<< "\n";													
  fileTXT << "translation: "
  << 0 << "\t"
  << 0 << "\t"
  << 0 << "\n";
  fileTXT << "scale: " << 1 << "\n";

  fileTXT << "\n";
  fileTXT.close();

  return true;
}
// write binary output
void writeRaw(std::string outputFilename, box mbbox, int dimension, resolution res, std::vector<vx_vertex_t> list)
{
  typedef unsigned char VoxelType;
  std::string voxeltype = "uchar";
  int nX, nY, nZ;
  		
		nX = (mbbox.upper.x - mbbox.lower.x )/ res.resX ;
		nY = (mbbox.upper.y - mbbox.lower.y) / res.resY ;
		nZ = (mbbox.upper.z - mbbox.lower.y) / res.resZ ;

	    int maxD;

  	if(mbbox.l == "x"){
		  maxD = nX;
	}
	else if(mbbox.l == "y"){
		maxD = nY;
	}
	else{
		maxD = nZ;
	}
	printf(" ** number of points %i\n", nX * nY * nZ);
  	printf(" ** volume dimensions %i %i %i\n", nX, nY, nZ);

    std::vector<VoxelType> rawData(maxD * maxD * maxD, 0);

  	// std::vector<int> rawXMax(nY * nZ, std::numeric_limits<int>::min());
  	// std::vector<int> rawXMin(nY * nZ, std::numeric_limits<int>::max());
  	// std::vector<int> rawYMax(nX * nZ, std::numeric_limits<int>::min());
  	// std::vector<int> rawYMin(nX * nZ, std::numeric_limits<int>::max());
  	std::vector<int> rawXMax(maxD * maxD, std::numeric_limits<int>::min());
  	std::vector<int> rawXMin(maxD * maxD, std::numeric_limits<int>::max());
  	std::vector<int> rawYMax(maxD * maxD, std::numeric_limits<int>::min());
  	std::vector<int> rawYMin(maxD * maxD, std::numeric_limits<int>::max());

	std::vector<int> realXMax(maxD * maxD, std::numeric_limits<int>::min());
  	std::vector<int> realXMin(maxD * maxD, std::numeric_limits<int>::max());
  	std::vector<int> realYMax(maxD * maxD, std::numeric_limits<int>::min());
  	std::vector<int> realYMin(maxD * maxD, std::numeric_limits<int>::max());


  	for (int j = 0; j < list.size(); ++j) {
  	    int ix = (list[j].x - mbbox.lower.x) / res.resX;
  	    int iy = (list[j].y - mbbox.lower.y) / res.resY;
  	    int iz = (list[j].z - mbbox.lower.z) / res.resZ;
       
  	    realXMax[iz * nY + iy] = std::max(ix, realXMax[iz * nY + iy]);
  	    realXMin[iz * nY + iy] = std::min(ix, realXMin[iz * nY + iy]);
  	    realYMax[iz * nX + ix] = std::max(iy, realYMax[iz * nX + ix]);
  	    realYMin[iz * nX + ix] = std::min(iy, realYMin[iz * nX + ix]);

		rawXMax[iz * maxD + iy] = std::max(ix, rawXMax[iz * maxD + iy]);
  	    rawXMin[iz * maxD + iy] = std::min(ix, rawXMin[iz * maxD + iy]);
  	    rawYMax[iz * maxD + ix] = std::max(iy, rawYMax[iz * maxD + ix]);
  	    rawYMin[iz * maxD + ix] = std::min(iy, rawYMin[iz * maxD + ix]);

  	}
	std::cout << rawXMax.size() << std::endl;
	
  	printf(" ** Done marking boundaries\n");

  	// write binary output
  	std::ofstream fileRAW((outputFilename + std::string(".raw")).c_str(),
  			      std::ios::out | std::ios::binary);

  	for (int iz = 0; iz < maxD; ++iz) {
  	    for (int iy = 0; iy < maxD; ++iy) {
  			for (int ix = 0; ix < maxD; ++ix) {
  		    	// int id = iz * nX * nY + iy * nX + ix;
				int id = iz * maxD * maxD + iy * maxD + ix;
  		   	    VoxelType value;
  		    	if (ix >= realXMin[iz * nY + iy] && ix <= realXMax[iz * nY + iy]) {
  						value = 1;
  		    	}
  		    	else if (iy >= realYMin[iz * nX + ix] && iy <= realYMax[iz * nX + ix]) {
  		    			value = 1;
  		    	}
  		   		 else {
  					   value = 0;
  		    	}
  		    rawData[id] = value;
  		    fileRAW.write(reinterpret_cast<char*>(&rawData[id]), sizeof(rawData[id]));
  			}
  	    }
  	}
  	fileRAW.close();
  	printf(" ** Done RAW\n");
}
