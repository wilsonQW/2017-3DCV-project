#!/bin/bash

# MODELPATH=/Users/hanmengjiao/2017-3dcv-project/model
# OSPRAYPATH=/Users/hanmengjiao/OSPRay/ospray-qwu-devel/build
MODELPATH=/home/sci/qwu/Projects/3DCV/model
OSPRAYPATH=/home/sci/qwu/software/ospray-qwu-28.03.2017/bin

# FILE=${MODELPATH}/wooddoll/wooddoll_00.obj
# SCALE=300
# TRANSX=250
# TRANSY=-11
# TRANSZ=0

# FILE=${MODELPATH}/teapot/teapot.obj
# SCALE=1
# TRANSX=150
# TRANSY=-11
# TRANSZ=0

FILE=${MODELPATH}/mango/source/MangoLow.obj
SCALE=4
TRANSX=-990
TRANSY=520
TRANSZ=40

# good viewport
# center of the tunnel
# -vp 667.492554 186.974228 76.008301                
# -vu 0.000000 1.000000 0.000000                     
# -vi 84.557503 188.199417 -38.148270                
# balcony
# -vp -1040.681274 550.735718 64.358147 \
# -vu 0.000000 1.000000 0.000000        \
# -vi -990.000000 520.000000 40.000004  \

${OSPRAYPATH}/ospGlutRenderer                          \
    ${MODELPATH}/crytek-sponza/sponza.obj              \
    --sun-dir -.3 -1 -.04                              \
    --renderer pt                                      \
    --sun-int 12                                       \
    -vp -1040.681274 550.735718 64.358147              \
    -vu 0.000000 1.000000 0.000000                     \
    -vi -990.000000 520.000000 40.000004               \
    --hdri-light 1 ${MODELPATH}/rnl_probe.pfm          \
    ${FILE}                                            \
    -ao 0                                              \
    --ambient .85 .9 1 .3                              \
    --multiObjects                                     \
    --translate ${TRANSX} ${TRANSY} ${TRANSZ}          \
    --scale ${SCALE} ${SCALE} ${SCALE}                 \
    # --maxFrame 100 example
